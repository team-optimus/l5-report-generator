Laravel 5 Report Generator
======================
What ?
------
This is a generic report generator based on query submitted by the user.

How?
------
Install via composer
```
composer require team-optimus/report-generator
```

Add Service Provider to `config/app.php` in `providers` section
```php
TeamOptimus\ReportGenerator\Providers\ReportGeneratorProvider::class,
```

Add a route in your web routes file:
```php 
Route::group(['prefix' => 'report-generator' ], function(){
    Route::get('/', ['as' => 'report_generator.index',  'uses' => '\TeamOptimus\ReportGenerator\Controllers\MainController@index']);
    Route::get('{id}/form', ['as' => 'report_generator.form',  'uses' => '\TeamOptimus\ReportGenerator\Controllers\MainController@form']);
    Route::get('{id}/download', ['as' => 'report_generator.download',  'uses' => '\TeamOptimus\ReportGenerator\Controllers\MainController@download']);
    Route::get('setup', ['as' => 'report_generator.setup',  'uses' => '\TeamOptimus\ReportGenerator\Controllers\MainController@setup']);
    Route::get('create', ['as' => 'report_generator.create',  'uses' => '\TeamOptimus\ReportGenerator\Controllers\MainController@create']);
    Route::post('store', ['as' => 'report_generator.store',  'uses' => '\TeamOptimus\ReportGenerator\Controllers\MainController@store']);
    Route::get('{id}/edit', ['as' => 'report_generator.edit',  'uses' => '\TeamOptimus\ReportGenerator\Controllers\MainController@edit']);
    Route::put('{id}', ['as' => 'report_generator.update',  'uses' => '\TeamOptimus\ReportGenerator\Controllers\MainController@update']);
    Route::delete('{id}', ['as' => 'report_generator.destroy',  'uses' => '\TeamOptimus\ReportGenerator\Controllers\MainController@destroy']);
});

Route::group(['prefix' => 'report/{report_id}/parameter' ], function(){
    Route::get('/', ['as' => 'report_parameter.index',  'uses' => '\TeamOptimus\ReportGenerator\Controllers\ParameterController@index']);
    Route::post('/', ['as' => 'report_parameter.store',  'uses' => '\TeamOptimus\ReportGenerator\Controllers\ParameterController@store']);
    Route::put('{id}', ['as' => 'report_parameter.update',  'uses' => '\TeamOptimus\ReportGenerator\Controllers\ParameterController@update']);
    Route::delete('{id}', ['as' => 'report_parameter.destroy',  'uses' => '\TeamOptimus\ReportGenerator\Controllers\ParameterController@destroy']);
});
```

Publish:
```
php artisan vendor:publish --provider="TeamOptimus\ReportGenerator\Providers\ReportGeneratorProvider" 
``` 

Overide blades on `resources/vendor/report-generator/`.

Go to `http://domain.com/report-generator` and check if working.

Troubleshooting
---------------

None for now.

