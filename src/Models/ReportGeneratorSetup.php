<?php
namespace TeamOptimus\ReportGenerator\Models;

use Illuminate\Database\Eloquent\Model;

class ReportGeneratorSetup extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $fillable = [
		'report_name',
		'description',
		'output_type',
		'report_query',
		'created_by',
		'last_update_by',
    ];

    public function parameters()
    {
    	return $this->hasMany('TeamOptimus\ReportGenerator\Models\ReportGeneratorParameters','report_generator_id','id');
    }
}
