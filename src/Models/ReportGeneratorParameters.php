<?php
namespace TeamOptimus\ReportGenerator\Models;

use Illuminate\Database\Eloquent\Model;

class ReportGeneratorParameters extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $fillable = [
		'report_generator_id',
		'label',
		'description',
		'data_type',
		'is_required',
		'default_value',
    ];

    public function setup()
    {
    	return $this->hasMany('TeamOptimus\ReportGenerator\Models\ReportGeneratorSetup','report_generator_id','id');
    }
}
