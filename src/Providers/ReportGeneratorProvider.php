<?php

namespace TeamOptimus\ReportGenerator\Providers;

use Illuminate\Support\ServiceProvider;

class ReportGeneratorProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/../routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('TeamOptimus\ReportGenerator\Controllers\MainController');

        $this->loadViewsFrom(__DIR__.'/../views', 'report_generator');

        if (method_exists($this, 'publishes')) {

            $this->publishes([
                   __DIR__.'/../views' => base_path('/resources/views/vendor/report_generator'),
            ], 'views');

            $this->publishes([
                   __DIR__.'/../migrations' => base_path('/database/migrations'),
            ], 'database');

            // $this->publishes([
            //     __DIR__.'/../../config/logviewer.php' => $this->config_path('logviewer.php'),
            // ]);

        }
    }
}
