<?php

namespace TeamOptimus\ReportGenerator\Controllers;

use Illuminate\Http\Request;
use TeamOptimus\ReportGenerator\Requests\ReportGeneratorParameterRequest;
use App\Http\Controllers\Controller;
use TeamOptimus\ReportGenerator\Models\ReportGeneratorParameters;

class ParameterController extends Controller
{
	public $param;

	public function __construct(ReportGeneratorParameters $param)
	{
		$this->param = $param;
	}

    public function setup()
    {
    	$results = $this->param->all();
		return view('report_generator::setup' , compact('results') );
    }

    public function store($setup_id,ReportGeneratorParameterRequest $request)
    {
        $user_id = auth()->check() ? auth()->user()->user_id : 0;

        $attributes = $request->all();
        $attributes['report_generator_id'] = $setup_id;
        $attributes['is_required'] = $request->is_required ? 1 : 0;
        $attributes['created_by'] = $user_id;
        $attributes['last_update_by'] = $user_id;

    	$data = $this->param->create($attributes);

        return response()->json([  
            'message' => 'Setup has been created.' ,
            'data' => $data
        ]);
    }

    public function update($setup_id, $id, ReportGeneratorParameterRequest $request)
    {
        $user_id = auth()->check() ? auth()->user()->user_id : 0;

        $find = $this->param->find($id);

        $attributes = $request->all();
        $attributes['is_required'] = $request->is_required ? 1 : 0;
        $attributes['last_update_by'] = $user_id;

        $find->update($attributes) ;

        return response()->json([  
            'message' => 'Setup has been updated.' ,
            'data' => $find
        ]);
    }

    public function destroy($setup_id,$id)
    {
        $this->param->whereId( $id)->delete();
        return response()->json([  
            'message' => 'Setup has been removed.' ,
        ]);
    }

    

}