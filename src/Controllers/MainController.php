<?php

namespace TeamOptimus\ReportGenerator\Controllers;

use Illuminate\Http\Request;
use TeamOptimus\ReportGenerator\Requests\ReportGeneratorSetupRequest;
use App\Http\Controllers\Controller;
use TeamOptimus\ReportGenerator\Models\ReportGeneratorSetup;
use DB;

class MainController extends Controller
{
	public $setup;

	public function __construct(ReportGeneratorSetup $setup)
	{
		$this->setup = $setup;
	}

    public function index()
    {
        $results = $this->setup->all();
        return view('report_generator::index' , compact('results') );
    }

    public function form($id)
    {
    	$template = $this->setup->find($id);
        $html = view('report_generator::includes.form_generator' , compact('template') )->render();
        return response()->json([  
            'message' => 'Setup has been created.' ,
            'data' => $template,
            'html' => $html
        ]);
    }

    public function create()
    {
        return view('report_generator::form');
    }

    public function setup()
    {
        $results = $this->setup->all();
		return view('report_generator::setup' , compact('results') );
    }

    public function store(ReportGeneratorSetupRequest $request)
    {
        $user_id = auth()->check() ? auth()->user()->user_id : 0;

        $attributes = $request->all();
        $attributes['created_by'] = $user_id;
        $attributes['last_update_by'] = $user_id;

    	$data = $this->setup->create($attributes);

        return response()->json([  
            'message' => 'Setup has been created.' ,
            'data' => $data
        ]);
    }

    public function edit($id)
    {
        $details = $this->setup->find($id);

        if(!$details) abort(404);

        return view('report_generator::form' ,compact('details') );
    }

    public function update($id, ReportGeneratorSetupRequest $request)
    {
        $user_id = auth()->check() ? auth()->user()->user_id : 0;

        $find = $this->setup->find($id);

        $attributes = $request->all();
        $attributes['last_update_by'] = $user_id;

        $find->update($attributes) ;

        return response()->json([  
            'message' => 'Setup has been updated.' ,
            'data' => $find
        ]);
    }

    public function destroy($id)
    {
        $this->setup->whereId( $id)->delete();
        return response()->json([  
            'message' => 'Setup has been removed.' ,
        ]);
    }

    public function download($id, Request $request)
    {
        $template = $this->setup->find($id);
        $filename = $template->report_name."_".time().".csv";
        $results = DB::select( $template->report_query , $request->param );
        $list = [];

        $headers = [
        'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
        'Content-type'        => 'text/csv',
        'Content-Disposition' => 'attachment; filename='.$filename ,
        'Expires'             => '0',
        'Pragma'              => 'public'
        ];


        foreach ($results as $key => $value) {
            $value = (array) $value;
            if(count($list) == 0){
                array_push($list, array_keys($value) );
            }
            array_push($list, $value);
        }


        $callback = function() use ($list) 
        {
            $fh = fopen('php://output', 'w');
            foreach ($list as $row) { 
                fputcsv($fh, $row);
            }
            fclose($fh);
        };

        return \Response::stream($callback, 200, $headers);   
    }

}