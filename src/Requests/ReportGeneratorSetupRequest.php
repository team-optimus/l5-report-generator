<?php

namespace TeamOptimus\ReportGenerator\Requests;

use App\Http\Requests\Request;

class ReportGeneratorSetupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'report_name' => "required|unique:report_generator_setups,report_name,".$this->segment(2),
            // 'description' => "required",
            'report_query' => "required",
        ];
    }
}
