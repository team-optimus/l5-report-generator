<?php

namespace TeamOptimus\ReportGenerator\Requests;

use App\Http\Requests\Request;

class ReportGeneratorParameterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label' => "required",
            'description' => "required",
            'data_type' => "required",
        ];
    }
}
