<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportGeneratorSetupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_generator_setups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('report_name');
            $table->text('description')->nullable();
            $table->text('report_query')->nullable();
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('last_update_by');
            $table->dateTime('last_update_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_generator_setups');
    }
}
