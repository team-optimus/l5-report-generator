<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportGeneratorParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_generator_parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_generator_id')->unsigned();
            $table->string('label');
            $table->text('description')->nullable();
            $table->string('data_type')->nullable();
            $table->integer('is_required');
            $table->text('default_value')->nullable();
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('last_update_by');
            $table->dateTime('last_update_date');

            $table->foreign('report_generator_id')
                ->references('id')
                ->on('report_generator_setups')
                ->onUpdate('RESTRICT')
                ->onDelete('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_generator_parameters');
    }
}
