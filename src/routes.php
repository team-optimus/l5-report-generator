<?php
	Route::group([ 'namespace' => "TeamOptimus\ReportGenerator\Controllers" ], function(){
	    Route::group(['prefix' => 'report-generator' ], function(){
	        Route::get('/', ['as' => 'report_generator.index',  'uses' => 'MainController@index']);
	        Route::get('{id}/form', ['as' => 'report_generator.form',  'uses' => 'MainController@form']);
	        Route::get('{id}/download', ['as' => 'report_generator.download',  'uses' => 'MainController@download']);
	        Route::get('setup', ['as' => 'report_generator.setup',  'uses' => 'MainController@setup']);
	        Route::get('create', ['as' => 'report_generator.create',  'uses' => 'MainController@create']);
	        Route::post('store', ['as' => 'report_generator.store',  'uses' => 'MainController@store']);
	        Route::get('{id}/edit', ['as' => 'report_generator.edit',  'uses' => 'MainController@edit']);
	        Route::put('{id}', ['as' => 'report_generator.update',  'uses' => 'MainController@update']);
	        Route::delete('{id}', ['as' => 'report_generator.destroy',  'uses' => 'MainController@destroy']);
	    });

	    Route::group(['prefix' => 'report/{report_id}/parameter' ], function(){
	        Route::get('/', ['as' => 'report_parameter.index',  'uses' => 'ParameterController@index']);
	        Route::post('/', ['as' => 'report_parameter.store',  'uses' => 'ParameterController@store']);
	        Route::put('{id}', ['as' => 'report_parameter.update',  'uses' => 'ParameterController@update']);
	        Route::delete('{id}', ['as' => 'report_parameter.destroy',  'uses' => 'ParameterController@destroy']);
	    });
	});